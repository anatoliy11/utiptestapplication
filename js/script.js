(function() {
  var lastUpdateIdentifier = '1';
  var drawingMode = false;
  var linesArray = [];
  var localStorageItems = [];
  var canvas = document.getElementById('drawingArea');
  canvas.setAttribute('width', getComputedStyle(canvas).width);
  canvas.setAttribute('height', getComputedStyle(canvas).height);
  var context = canvas.getContext('2d');

  var currentColorIndex = 0;
  var currentWidthIndex = 0;
  var colorItems = ['red', 'blue', 'black'];
  var widthItems = [2, 4, 6];

  colorItems.forEach(function(item, index) {
    var button = document.createElement('a');
    button.innerText = 'Color ' + index;
    button.dataset.color = index;
    button.onclick = setPenStyle;
    colorPanel.appendChild(button);
  });

  widthItems.forEach(function(item, index) {
    var button = document.createElement('a');
    button.innerText = 'Width ' + index;
    button.dataset.width = index;
    button.onclick = setPenStyle;
    widthPanel.appendChild(button);
  });

  function render() {
    if (window.localStorage['lastUpdateIdentifier'] !== lastUpdateIdentifier) {
      localStorageItems = JSON.parse(window.localStorage.getItem('linesArray')) || [];
      lastUpdateIdentifier = window.localStorage['lastUpdateIdentifier'];
    }

    var resultArray = localStorageItems.concat(linesArray);
    context.clearRect(0 ,0, canvas.width, canvas.height);
    for (var i = 0, l = resultArray.length; i < l; i++) {
      var line = resultArray[i];
      context.beginPath();
      context.strokeStyle = colorItems[line.color];
      context.lineWidth = widthItems[line.width];
      var path = line.path;
      context.moveTo(path[0].x, path[0].y);
      for (var j = 1, k = path.length; j < k; j++) {
        context.lineTo(path[j].x, path[j].y);
        context.stroke();
      }
      context.closePath();
    }

    context.font = '12px Arial';
    context.fillStyle = '#000';
    context.fillText('Color:', 5, 17);
    context.fillText('Width:', 5, 32);
    context.fillText(widthItems[currentWidthIndex] + 'px', 45, 32);
    context.fillStyle = colorItems[currentColorIndex];
    context.fillRect(45, 8, 10, 10);

    window.requestAnimationFrame(render);
  }

  window.requestAnimationFrame(render);

  canvas.onmousedown = function(event) {
    drawingMode = true;
    linesArray.push({
      color: currentColorIndex,
      width: currentWidthIndex,
      path: [{
        x: event.pageX - this.offsetLeft,
        y: event.pageY - this.offsetTop
      }]
    });
  };

  canvas.onmouseup = function() {
    drawingMode = false;
  };

  canvas.onmousemove = function(event) {
    if (drawingMode) {
      linesArray[linesArray.length - 1]['path'].push({
        x: event.pageX - this.offsetLeft,
        y: event.pageY - this.offsetTop
      });
    }
  };

  clear.onclick = function() {
    linesArray.length = 0;
    localStorageItems.length = 0;
  };

  save.onclick = function() {
    var resultArray = localStorageItems.concat(linesArray);
    window.localStorage.setItem('linesArray', JSON.stringify(resultArray));
    linesArray.length = 0;
    window.localStorage['lastUpdateIdentifier'] = Math.random() + '';
  };

  function setPenStyle(event) {
    currentColorIndex = event.currentTarget.dataset.color || currentColorIndex;
    currentWidthIndex = event.currentTarget.dataset.width || currentWidthIndex;
  }
})();